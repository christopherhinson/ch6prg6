//Chris Hinson
//Period 4
//Ch 6 Program 6

import java.util.Scanner;

public class main {
    public static void main(String args[])
    {
        Scanner k = new Scanner(System.in);
        System.out.println("Score 1?");
        double score1 = k.nextDouble();
        System.out.println("Score 2?");
        double score2 = k.nextDouble();
        System.out.println("Score 3?");
        double score3 = k.nextDouble();

        TestScores testScores = new TestScores(score1,score2,score3);
        System.out.println("Score average is : " + testScores.getAverage());
    }
}
